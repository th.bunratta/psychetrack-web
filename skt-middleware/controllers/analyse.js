const axios = require('axios')
module.exports = {
    analyseHWData: async(req, res, next) => {
        const { data } = await axios.get("https://exceed.superposition.pknn.dev/data/ccat88")
        const hwModel = data
        const active_time = hwModel.active_time
        const light_time = hwModel.light_time
        let tend_psyco_score = 0
      for(const i of Array(4).keys()){
        //darkplace totalscore
        if(active_time[i][0]>3 && active_time[i][0]<5){
          tend_psyco_score+=1
        }
        else if(active_time[i][0]>5 && active_time[i][0]<7){                                                                           
          tend_psyco_score+=2
        }
        else if (active_time[i][0]>7) {
          tend_psyco_score+=3
        }


        //moving
        if(light_time[i][0]>3 && light_time[i][0]<5){
          tend_psyco_score+=1
        }
        else if(light_time[i][0]>5 && light_time[i][0]<7){                                                                           
          tend_psyco_score+=2
        }
        else if(light_time[i][0]>7) {
          tend_psyco_score+=3
        }
      }

      //check psysco
      if(tend_psyco_score<5){
        hwModel.patient_status= 'normal'
      }
      else if(tend_psyco_score<10){
        hwModel.patient_status= 'mild'
      }
      else if(tend_psyco_score<10){
        hwModel.patient_status='moderate'
      }
      else if(tend_psyco_score<10){
        hwModel.patient_status='severe'
        hwModel.emergency=1
      }
      await axios.post("https://exceed.superposition.pknn.dev/data/ccat88", {
          data: hwModel
      })
      res.status(200).json({ status: hwModel.patient_status })
    }
}