const axios = require('axios')
module.exports = {
    getData: async(req, res, next) => {
        const response = await axios.get("https://exceed.superposition.pknn.dev/data/ccat88")
        res.status(200).json(response.data)
    },
    setData: async(req, res, next) => {
       const { finalProj } = req.body

       const response = await axios.post("https://exceed.superposition.pknn.dev/data/ccat88", {
            data: {
                finalProj
            }
        },  { headers: { "Content-Type": "application/json" }})
        res.status(200).json(response.data)
    }
}