const lineNotif = require('../services/notifications')

module.exports = {
    handle: async(req, res, next) => {
        switch (req.body.events[0].type) {
            case 'follow':
                lineNotif.pushNotification("คุณได้ลงทะเบียนเข้าสู่ระบบ PsycheTrack", req.body.events[0].source.userId)
                break
        }
        console.log(req.body.events)
    }
}