const axios = require('axios')
const QuestionModel = require('../models/questions')
const AnswerModel = require('../models/answers')

module.exports = {
    getQuestions: async(req, res, next) => {
            //const { uid } = req.user
            // const { route } = req.body
            let foundTasks = await QuestionModel.findAll({raw: true}) //where: {category_id: route}})
            let editedTasks = []
            for (var index in foundTasks) {
              let value = foundTasks[index]
            //   const config = { raw: true, where: {
            //     [Op.and]: [{ user_id: uid }, { task_id: value.uid }] 
            //   }};
              delete value.uid;
              try {
                // let allSubmissions = await SubmissionModel.findAll(config);
                // allSubmissions.forEach(function(v2){ delete v2.task_id; delete v2.user_id });
                // // let taskScore = await TaskScoreModel.findOne(config);
                // value.submissions = allSubmissions
                // if (taskScore) {
                //   value.score = taskScore.score
                //   value.passed = taskScore.passed
                // }
              }
              catch (error) {
                console.log(error)
              }
              editedTasks[index] = value
            }        
            const response = {
              questions: editedTasks
            }
            res.status(200).json(response)
            console.log("done")
    },
    postAnswers: async(req, res, next) => {
        const { answers } = req.body
        for (let answer of answers) {
          if (answer.answer !== undefined) {
            AnswerModel.create({
              question_id: answer.question_id, answer: answer.answer, category: ' '
            })
          }
        }
    }
}