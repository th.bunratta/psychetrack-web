var cron = require('node-cron')

const notifier = require('../services/notifications')
const hardware = require('../services/hw')
var sequelize = require('sequelize')

async function notify() {
   let hwData = await hardware.getData()
   if (hwData.emergency === 1) {
    notifier.pushNotification("นายสมชาย ทะลุฟ้า แสดงอาการของโรคซึมเศร้าในระดับฉุกเฉิน โปรดพิจารณาให้ความช่วยเหลือ", "Ued9a50dc8a54b2eab0226677c36bf077")//U3dd1191f1fb9faa9eaa7c35ea2779951")
    hwData.emergency = 0
    await hardware.setData(hwData)
   }
   
   
}

function schedule() {
    cron.schedule('*/2 * * * * *', async () => {
        try {
            await notify()
        }
        catch (error) {
            console.log("Error updating the hardware data... " + error)
        }
    })
    console.log("Started")
}

module.exports = {
    schedule: schedule
}
