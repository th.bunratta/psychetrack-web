var router = require('express').Router()
var hwHandler = require('./hardware')
var qHandler = require('../../controllers/questions')
var authHandler = require('./auth')

router.get("/", qHandler.getQuestions)
router.post("/", qHandler.postAnswers)

module.exports = router
