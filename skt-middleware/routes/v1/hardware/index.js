var router = require('express').Router()
const hardware = require('../../../controllers/hardware')

router.post('/in', hardware.setData)
router.get('/out', hardware.getData)
module.exports = router