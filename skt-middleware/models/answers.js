const sequelize = require("sequelize")
const db = require("../db-init.js")

module.exports = db.sequelize.define(
    "psych_questionnaire_answers",
    {
        id: {
            type: sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        question_id: { type: sequelize.INTEGER },
        answer: { type: sequelize.INTEGER },
        category: { type: sequelize.STRING }
    }, { timestamps: false }
)
