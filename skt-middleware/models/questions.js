const sequelize = require("sequelize")
const db = require("../db-init.js")

module.exports = db.sequelize.define(
    "psych_questionnaire",
    {
        id: {
            type: sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        }, 
        question: { type: sequelize.TEXT },
        category: { type: sequelize.STRING }
    }, { timestamps: false }
)
