const lineService = require('../services/line')
module.exports = {
    pushNotification: (message, userId) => {
        const ps = new lineService()
        ps.initialize().then ((client) => {
        lineService.pushMessage(client, message, userId)
        }) 
    }
}