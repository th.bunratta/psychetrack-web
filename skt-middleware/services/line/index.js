const axios = require('axios')
const lineConfig = require('../../config/line-passport')
const line = require('@line/bot-sdk')

module.exports = class LineService {
    client = undefined
    async getAccessToken() {
        const req_params = new URLSearchParams()
        req_params.append('grant_type', "client_credentials")
        req_params.append('client_id', lineConfig.channel_id)
        req_params.append('client_secret', lineConfig.channel_secret)

        const res = await axios.post('https://api.line.me/v2/oauth/accessToken', req_params, {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        })
        return res.data.access_token
    }
    async initialize() {
        const token = await this.getAccessToken()
        LineService.client = new line.Client({
            channelAccessToken: token
        })
        return LineService.client
    }
    static async pushMessage(client, msg, userId) {
        //const userId = "Ued9a50dc8a54b2eab0226677c36bf077"

        const message = {
          type: 'text',
          text: msg
        };
      
        client.pushMessage(userId, message)
          .then((res) => {
            console.log(res)
          })
          .catch((err) => {
          });
    }
}