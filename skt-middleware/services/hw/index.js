const axios = require('axios')
module.exports = {
    getData: async(req, res, next) => {
        return (await axios.get("https://exceed.superposition.pknn.dev/data/ccat88")).data
        
    },
    setData: async(req) => {

       return await axios.post("https://exceed.superposition.pknn.dev/data/ccat88", {
            data: req
        },  { headers: { "Content-Type": "application/json" }})
    }
}