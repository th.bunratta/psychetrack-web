const app = require('express')()
const bodyParser = require('body-parser')
const cors = require('cors')
const lineService = require('./services/line')
const cron_runner = require('./cron/summarizer')
var v1_api = require('./routes/v1')
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/', v1_api)
app.use('/v1', v1_api)

app.listen(3000, function () {
  console.log("Server started on port " + 3000)
} )

cron_runner.schedule()
