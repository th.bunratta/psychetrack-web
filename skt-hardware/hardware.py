
from machine import Pin,ADC,PWM
import time
import math
import random
from _thread import start_new_thread as thread
import network
import json
import urequests

x_pin = 32
y_pin = 34
z_pin = 33

x_axis = ADC(Pin(x_pin))
y_axis = ADC(Pin(y_pin))
z_axis = ADC(Pin(z_pin))
count_status = 0

#-------------------------------------- light -------------------
#def Openlight():
  #led = Pin(5, Pin.OUT)
  #led.value(0)
#def Closelight():
  #led = Pin(5, Pin.OUT)
  #led.value(1)

#----------------- Network ---------------------


ssid = 'MY15'
pwd='0956800358'
net = network.WLAN(network.STA_IF)
net.active(True)
while not net.isconnected():
  net.connect(ssid,pwd)
  print('Connecting')
  time.sleep(2)
print('Connected')

url = "https://exceed.superposition.pknn.dev/data/ccat88"
headers = {"content-type":"application/json"}
is_free = True

def shift_data(data,new_data):
  data[0] = data[1]
  data[1] = data[2]
  data[2] = data[3]
  data[3] = new_data
  return data

def sent_data(data,url,headers):
  global is_free
  j_c = json.dumps({'data' : data })
  while is_free == False:
    time.sleep(0.1)
  is_free = False
  print("Senting ",data)
  r_c = urequests.post(url,data=j_c,headers=headers)
  is_free = True
  time.sleep(0.3)
  print("status : ",r_c.json())

def receive_data(url,headers):
  global is_free
  while is_free == False:
    time.sleep(0.1)
  print("receiving")
  is_free = False
  e_data = urequests.get(url).json()
  is_free = True
  print("received : ",e_data)
  time.sleep(0.5)
  return e_data


def wait_request(url,headers):
  global active_time
  global light_time
  while True :
    data = receive_data(url,headers)
    if data['request'] == 1 :
      print("receive request")
      data['active_time'] = shift_data(data['active_time'],active_time)
      data['light_time'] = shift_data(data['light_time'],light_time)
      data['request'] = 0
      data['active_time_avg'] = sum(data['active_time'])/4
      data['light_time_avg'] = sum(data['light_time'])/4
      sent_data(data,url,headers)
      #while(receive_data(url,headers) != data):
      #  print("data interupt")
      #  sent_data(data,url,headers)
      #  time.sleep(0.3)
      print("sent data")
      active_time = 0
      light_time = 0
    time.sleep(0.5)
    
    
def sent_emergency(url,headers):
  print("senting emergency")
  data = receive_data(url,headers)
  data['emergency'] = 1
  sent_data(data,url,headers)
  print("send emergency")
  
thread(wait_request,(url,headers))
#------------------ Button ---------------------
def wait_press(b):
  while True:
    if b.value() == 0:
      return True
    else:
      time.sleep(0.1)
def wait_relese(b):
  while True:
    if b.value() == 1:
      return True
    else:
      time.sleep(0.1)
      
press_status = False

#--------------------------------------- Button check -------------------------------
Button_check = 0
def button_check():
  global Button_check
  global press_status
  global count_status
  button_pin = 19
  button = Pin( button_pin , Pin.IN )
  while True:
    wait_press(button)
    wait_relese(button)
    press_status = True
    Button_check = 1
    print("Pressed")
    
thread(button_check,())
#--------------------------- Emergency Function ----------------------

def count_10_sec():
  global count_status
  for i in range(0,100):
    time.sleep(0.1)
    if count_status == 0:
      return True
  count_status = 0
  return True
  
def Blinking():
  led = Pin(5, Pin.OUT)
  global count_status
  print("blinking")
  while(1):
    led.value(0)
    time.sleep(0.1)
    led.value(1)
    time.sleep(0.1)
    if count_status == 0 :
      return True
  
def confirm():
  global count_status
  global Button_check
  thread(count_10_sec,())
  while True :
    if Button_check == 1 and count_status == 1:
      Button_check = 0
      return True
    elif count_status == 0:
      return False
    time.sleep(0.1)



def Emergency():
  led = Pin(5, Pin.OUT)
  global count_status
  global Button_check
  while True:
    if Button_check == 1:
      print("Emergency request")
      thread(Blinking,())
      Button_check = 0
      count_status = 1
      if confirm() == True:
        #sending data and blinking
        count_status = 0
        time.sleep(0.5)
        led.value(0)
        sent_emergency(url,headers)
        led.value(1)
        print("Emergency receive")
      else:
        print("Emergency denied")
    time.sleep(0.1)
thread(Emergency,())
#------------------------------------------------ Sensor measure --------------------------------------------
active_time = 0.0
light_time = 0.0
def check_light():
  light_pin = 39
  light = ADC(Pin(light_pin))
  global light_time
  while True:
    if light.read() > 4000:
      light_time = light_time + 0.1
      #print("light : ",light_time," : ",light.read())
      time.sleep(0.1)
    else:
      time.sleep(0.1)
thread(check_light,())



while True :
  temp_x  = x_axis.read()
  temp_y  = y_axis.read()
  temp_z  = z_axis.read()
  new_x = x_axis.read()
  new_y = y_axis.read()
  new_z = z_axis.read()
  #-------- Active time recorder
  if temp_x != new_x or temp_y != new_y or temp_z != new_z:
    active_time = active_time + 0.1
    print("x : ",new_x - temp_x , " y : ",new_y - temp_y," z : ",new_z-temp_z," size : ",math.sqrt((new_x - temp_x)*(new_x - temp_x)+(new_y - temp_y)*(new_y - temp_y)+(new_z-temp_z)*(new_z-temp_z)))
    time.sleep(0.3)
    
    #print(active_time)
    
    
    





